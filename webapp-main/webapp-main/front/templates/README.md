Ces fichiers sont des templates HTML.

C'est à dire qu'en plus de remplir leur rôle de fichier HTML (et donc de page du site), il est possible d'utiliser des variables à l'intérieur d'entre eux.

Selon leur valeur des variables au moment de leur injection dans le template, la page affichée ne sera pas la même.

```html
<ul>
    {% for recipe in latest_recipes %}
        <li>
        <strong>{{ recipe.name }}</strong><br>
        Ingredients: {{ recipe.ingredients|join(', ') }}<br>
        Created at: {{ recipe.created_at }}
        </li>
    {% endfor %}
</ul>
```

Pourra être <ul></ul> si la variable latest_recipes est une liste vide, ou bien cela si elle contient un élément par exemple :

```
<ul>
        <li>
        <strong>Pâtes au saumon</strong><br>
        Ingredients: pate, saumon, crème fraiche<br>
        Created at: 2023-09-17T22:11:44.301+00:00
        </li>
</ul>
```

Les routes dans le serveur flask génère le fichier à chaque fois que la page est appelée via la fonction render_template('fichier.html').

Les valeurs des variables utilisées dans le template lui sont automatiquement passées selon le contexte d'exécution.
