from flask import Flask, render_template, redirect, url_for, request
from requests import get, post  # Assuming you'll make requests to your API
from os import environ

"""
Cette application web basée sur Flask permet aux utilisateurs de consulter et d'ajouter des recettes.
Elle interagit avec une API pour gérer les données de recettes.

Routes :
- '/' : Affiche les recettes les plus récentes ainsi qu'une recette aléatoire.
- '/add_recipe' : Permet aux utilisateurs d'ajouter de nouvelles recettes.

Utilisation :
1. Démarrez le backend
2. Définissez la variable d'environnement 'BACKEND_URL' avec le point de terminaison de l'API.
3. Exécutez ce script pour démarrer l'application web.
"""

def create_app():
    app = Flask(__name__)
    return app

app = create_app()
api_url = environ.get("BACKEND_URL", "http://127.0.0.1:5000/recipes")

from random import choice

@app.route('/', methods=['GET'])
def index():
    try:
        # Make a request to your API to get the recipes
        try:
            response = get(api_url)
            if response.status_code == 200:
                recipes = response.json()
            else:
                recipes = []
        except Exception as e:
            recipes = []

        # Sort recipes by created_at in descending order to get the latest
        latest_recipes = sorted(recipes, key=lambda x: x['created_at'], reverse=True)[:3]

        # Select a random recipe
        random_recipe = None

        if recipes:
            random_recipe = choice(recipes)

        return render_template('index.html', latest_recipes=latest_recipes, random_recipe=random_recipe)
    except Exception as e:
        return f"An error occurred: {str(e)}"

@app.route('/add_recipe', methods=['GET', 'POST'])
def add_recipe():
    if request.method == 'POST':
        # Process the form data and add the recipe using your API
        data = {
            'name': request.form['name'],
            'ingredients': request.form['ingredients'].split(',')  # Split ingredients by commas
        }
        try:
            response = post(api_url, json=data)
            if response.status_code == 201:
                # Recipe added successfully, redirect to the homepage or a confirmation page
                return redirect('/')
            else:
                # Handle errors, e.g., display an error message
                return "An error occurred while adding the recipe."
        except:
            return "An error occurred while adding the recipe."

    return render_template('add_recipe.html')

if __name__ == '__main__':
    app.run(port=5001, debug=environ.get("DEBUG_MODE", True))
