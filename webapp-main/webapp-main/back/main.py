from flask import Flask, jsonify, request
from recipes import create_recipe, get_recipes, get_recipe, update_recipe, delete_recipe
from database import connect_to_db
from os import environ

"""
Cette application permet la gestion des recettes en utilisant Flask comme framework web et MongoDB comme base de données.

Fonctions :
- create_recipe(data, collection) : Ajoute une nouvelle recette dans la base de données.
- get_recipes(collection) : Récupère la liste de toutes les recettes.
- get_recipe(id, collection) : Récupère une recette spécifique par son ID.
- update_recipe(id, data, collection) : Met à jour une recette existante.
- delete_recipe(id, collection) : Supprime une recette.

Routes :
- '/recipes' [POST] : Crée une nouvelle recette.
- '/recipes' [GET] : Récupère la liste de toutes les recettes.
- '/recipes/<string:id>' [GET] : Récupère une recette spécifique par son ID.
- '/recipes/<string:id>' [PUT] : Met à jour une recette spécifique par son ID.
- '/recipes/<string:id>' [DELETE] : Supprime une recette spécifique par son ID.

Dépendances :
- Flask : Framework web pour Python.
- pymongo : Pilote Python pour MongoDB.

Utilisation :
1. Assurez-vous d'avoir une instance MongoDB en cours d'exécution.
2. Définissez les variables d'environnement 'DATABASE_URL', 'DATABASE_NAME' et 'COLLECTION_NAME' pour configurer la connexion à la base de données.
3. Exécutez ce script pour lancer l'application web.

Exemple d'utilisation :
   DATABASE_URL = "mongodb://localhost:27017/"
   DATABASE_NAME = "recipes_db"
   COLLECTION_NAME = "recipes"
"""

def create_app():
    app = Flask(__name__)
    return app

app = create_app()
db_client = connect_to_db(environ.get("DATABASE_URL", "mongodb://localhost:27017/"))
db = db_client[environ.get("DATABASE_NAME", "recipes_db")]
collection = db[environ.get("COLLECTION_NAME", "recipes")]

@app.route('/recipes', methods=['POST'])
def create_recipe_route():
    try:
        data = request.json
        inserted_recipe = create_recipe(data, collection)
        return jsonify(inserted_recipe), 201
    except Exception as e:
        app.logger.error(f"Error creating recipe: {str(e)}")
        return jsonify({"error": "An error occurred"}), 500

@app.route('/recipes', methods=['GET'])
def get_recipes_route():
    try:
        recipes = get_recipes(collection)
        return jsonify(recipes)
    except Exception as e:
        app.logger.error(f"Error fetching recipes: {str(e)}")
        return jsonify({"error": "An error occurred"}), 500

@app.route('/recipes/<string:id>', methods=['GET'])
def get_recipe_route(id):
    try:
        recipe = get_recipe(id, collection)
        if recipe:
            return jsonify(recipe)
        else:
            return ('', 404)
    except Exception as e:
        app.logger.error(f"Error fetching recipe: {str(e)}")
        return jsonify({"error": "An error occurred"}), 500

@app.route('/recipes/<string:id>', methods=['PUT'])
def update_recipe_route(id):
    try:
        data = request.json
        updated_recipe = update_recipe(id, data, collection)
        return jsonify(updated_recipe)
    except Exception as e:
        app.logger.error(f"Error updating recipe: {str(e)}")
        return jsonify({"error": "An error occurred"}), 500

@app.route('/recipes/<string:id>', methods=['DELETE'])
def delete_recipe_route(id):
    try:
        delete_recipe(id, collection)
        return ('', 204)
    except Exception as e:
        app.logger.error(f"Error deleting recipe: {str(e)}")
        return jsonify({"error": "An error occurred"}), 500

if __name__ == '__main__':
    app.run(debug=environ.get("DEBUG_MODE", True))
