from bson.objectid import ObjectId
from datetime import datetime

"""
Fonctions de Gestion de Recettes pour MongoDB

Ce module contient des fonctions pour interagir avec une base de données MongoDB afin de créer, lire, mettre à jour et supprimer des recettes.

Fonctions :
- create_recipe(data, recipes_collection) : Crée une nouvelle recette dans la collection spécifiée.
- get_recipes(recipes_collection) : Récupère la liste de toutes les recettes sous forme de dictionnaires.
- get_recipe(id, recipes_collection) : Récupère une recette spécifique par son ID.
- update_recipe(id, data, recipes_collection) : Met à jour les détails d'une recette existante.
- delete_recipe(id, recipes_collection) : Supprime une recette spécifique par son ID.

Dépendances :
- pymongo : Pilote Python pour MongoDB.
- bson.objectid : Module pour gérer les identifiants d'objets MongoDB.
- datetime : Module pour gérer les dates et heures.

Utilisation :
1. Importez ce module dans votre script Python.
2. Utilisez les fonctions fournies pour interagir avec votre base de données MongoDB.

Exemple d'utilisation :
   # Connexion à la base de données
   client = connect_to_db("mongodb://localhost:27017/")
   db = client["my_database"]
   collection = db["my_collection"]
   
   # Création d'une nouvelle recette
   new_recipe_data = {
       "name": "Nouvelle Recette",
       "ingredients": ["Ingrédient 1", "Ingrédient 2"]
   }
   new_recipe = create_recipe(new_recipe_data, collection)
"""

def create_recipe(data, recipes_collection):
    new_recipe = {
        "name": data['name'],
        "ingredients": data['ingredients'],
        "created_at": datetime.utcnow()
    }
    result = recipes_collection.insert_one(new_recipe)
    inserted_id = str(result.inserted_id)
    inserted_recipe = recipes_collection.find_one({"_id": result.inserted_id})
    inserted_recipe['_id'] = inserted_id
    return inserted_recipe

def get_recipes(recipes_collection):
    all_recipes = list(recipes_collection.find({}, {'_id': 1, 'name': 1, 'ingredients': 1, 'created_at': 1}))
    return [{'_id': str(recipe['_id']), 'name': recipe['name'], 'ingredients': recipe['ingredients'], 'created_at': recipe['created_at']} for recipe in all_recipes]

def get_recipe(id, recipes_collection):
    recipe = recipes_collection.find_one({"_id": ObjectId(id)})
    return {'_id': str(recipe['_id']), 'name': recipe['name'], 'ingredients': recipe['ingredients'], 'created_at': recipe['created_at']}

def update_recipe(id, data, recipes_collection):
    updated_recipe = {
        "name": data['name'],
        "ingredients": data['ingredients']
    }
    recipes_collection.update_one({"_id": ObjectId(id)}, {"$set": updated_recipe})
    updated_recipe['_id'] = id  # Add the ID to the updated recipe

    # Retrieve the created_at field and add it to the updated_recipe
    recipe = recipes_collection.find_one({"_id": ObjectId(id)}, {'_id': 0, 'created_at': 1})
    updated_recipe['created_at'] = recipe.get('created_at')

    return updated_recipe

def delete_recipe(id, recipes_collection):
    return recipes_collection.delete_one({"_id": ObjectId(id)})
