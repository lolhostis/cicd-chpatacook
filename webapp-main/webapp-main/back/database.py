from pymongo import MongoClient
from pymongo.errors import ServerSelectionTimeoutError

"""
Connexion à la Base de Données MongoDB

Ce module permet d'établir une connexion à une base de données MongoDB en utilisant le pilote 'pymongo'.

Fonctions :
- connect_to_db(database_connection_string) : Établit une connexion à la base de données en utilisant la chaîne de connexion spécifiée.

Dépendances :
- pymongo : Pilote Python pour MongoDB.

Exemple d'utilisation :
   database_connection_string = "mongodb://localhost:27017/"
   client = connect_to_db(database_connection_string)
"""

def connect_to_db(database_connection_string):
    try:
        client = MongoClient(database_connection_string, serverSelectionTimeoutMS=5000)
        client.server_info()
        return client
    except ServerSelectionTimeoutError as e:
        raise Exception(f"Failed to connect to the database: {e}")
