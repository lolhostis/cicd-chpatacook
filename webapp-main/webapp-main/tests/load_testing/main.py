from locust import HttpUser, task, constant
from os import environ

# Définition de l'utilisateur Locust
class QuickstartUser(HttpUser):
    wait_time = constant(0)  # Temps d'attente constant entre les requêtes (en secondes)
    host = environ.get("BACKEND_URL", "http://127.0.0.1:5000/recipes")  # URL de l'application backend

    # Tâche de test (sera exécutée 6 fois plus souvent que les autres ; on peut pondérer les tâches)
    @task(6)
    def test_get_method(self):
        self.client.get("")  # Envoi d'une requête GET (dans ce cas, la requête est envoyée à la racine de l'URL)

    # TODO: Ajouter une tâche pour créer une recette, avec un poids de 3