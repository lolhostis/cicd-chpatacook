import sys
sys.path.append("../../back")  # Add the parent directory to the Python path


from recipes import *
from unittest.mock import MagicMock
from datetime import datetime

def test_create_recipe():
    # Define a mock data
    data = {
        'name': 'Spaghetti Bolognese',
        'ingredients': ['pasta', 'tomato sauce', 'ground beef']
    }

    # Define a mock result from insert_one
    mock_result = MagicMock()
    mock_result.inserted_id = ObjectId()

    # Define a mock inserted recipe
    inserted_recipe = {
        '_id': mock_result.inserted_id,
        'name': data['name'],
        'ingredients': data['ingredients']
    }

    # Mock the recipes collection
    recipes_collection = MagicMock()
    recipes_collection.insert_one.return_value = mock_result
    recipes_collection.find_one.return_value = inserted_recipe

    # Call the function
    returned_recipe = create_recipe(data, recipes_collection)
    
    # Assert that the function returned the correct recipe 
    # TODO: vérifier que le nom de la recette renvoyée correspond à ce qui est attendu
    assert returned_recipe['name'] == data['name']
    # TODO: vérifier que les ingrédients de la recette renvoyée correspondent à ce qui est attendu
    assert returned_recipe['ingredients'] == data['ingredients']
    # TODO: vérifier que l'id de la recette renvoyée correspond à ce qui est attendu
    assert returned_recipe['_id'] == inserted_recipe['_id']

def test_get_recipes():
    # Define a mock list of recipes  
    mock_recipes = [
        {'_id': ObjectId(), 'name': 'Spaghetti Bolognese', 'ingredients': ['pasta', 'tomato sauce', 'ground beef'], 'created_at': datetime.utcnow()},
        {'_id': ObjectId(), 'name': 'Caesar Salad', 'ingredients': ['lettuce', 'croutons', 'parmesan'], 'created_at': datetime.utcnow()}
    ]

    # Mock the recipes collection
    recipes_collection = MagicMock()
    recipes_collection.find.return_value = mock_recipes

    # Call the function
    recipes = get_recipes(recipes_collection)

    # Assert that the function returned the correct list of recipes
    # TODO: vérifier que la longueur de la liste renvoyée correspond à la longueur de la liste envoyée
    assert len(recipes) == len(mock_recipes)
    # TODO: vérifier que le nom de la première recette renvoyée correspond au nom de la première recette envoyée
    assert recipes[0]['name'] == mock_recipes[0]['name'] 

def test_get_recipe():
    # Define a mock recipe ID
    mock_id = str(ObjectId())

    # Define a mock recipe
    mock_recipe = {'_id': ObjectId(mock_id), 'name': 'Spaghetti Bolognese', 'ingredients': ['pasta', 'tomato sauce', 'ground beef'], 'created_at': datetime.utcnow()}

    # Mock the recipes collection
    recipes_collection = MagicMock()
    recipes_collection.find_one.return_value = mock_recipe

    # Call the function
    recipe = get_recipe(mock_id, recipes_collection)

    # Assert that the function returned the correct recipe
    # TODO: vérifier que le nom de la recette renvoyée correspond au nom de recette envoyée  
    assert recipe['name'] == mock_recipe['name']
    # TODO: vérifier que les ingrédients de la recette renvoyée correspondent à ce qui est attendu
    assert recipe['ingredients'] == mock_recipe['ingredients']

def test_update_recipe():
    # Define a mock recipe ID
    mock_id = str(ObjectId())

    # Define a mock data
    data = {
        'name': 'Spaghetti Carbonara',
        'ingredients': ['pasta', 'eggs', 'bacon']
    }

    # Mock the recipes collection
    recipes_collection = MagicMock()

    # Call the function
    updated_recipe = update_recipe(mock_id, data, recipes_collection)

    # Assert that the function returned the correct updated recipe
    # TODO: vérifier que le nom de la recette modifiée correspond au nom de recette envoyée
    assert updated_recipe['name'] == data['name']
    # TODO: vérifier que les ingrédients de la recette modifiée correspondent à ce qui est attendu
    assert updated_recipe['ingredients'] == data['ingredients']

def test_delete_recipe():
    # Define a mock recipe ID
    mock_id = str(ObjectId())

    # Mock the recipes collection
    recipes_collection = MagicMock()

    # Call the function
    delete_recipe(mock_id, recipes_collection)

    # Assert that delete_one was called with the correct ID
    recipes_collection.delete_one.assert_called_with({"_id": ObjectId(mock_id)})
