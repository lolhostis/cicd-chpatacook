import sys
sys.path.append("../../back")  # Ajoute le répertoire parent au chemin d'accès Python

from recipes import *
from database import *
from unittest.mock import MagicMock
from os import environ

# Fonction pour obtenir la collection de test
def get_test_collection():
    db_client = connect_to_db(environ.get("DATABASE_URL", "mongodb://localhost:27017/"))
    db = db_client[environ.get("DATABASE_NAME", "recipes_db")]
    collection = db[environ.get("COLLECTION_NAME", "recipes")]
    return collection

# Test de la fonction de création de recette
def test_create_recipe():
    recipes_collection = get_test_collection()
    
    # Données d'exemple pour la recette
    sample_data = {
        "name": "Recette Exemple",
        "ingredients": ["Ingrédient 1", "Ingrédient 2"]
    }

    # Appel de la fonction de création de recette
    inserted_recipe = create_recipe(sample_data, recipes_collection)

    # Vérification de la présence des champs 'name' et 'ingredients' dans la recette insérée
    assert 'name' in inserted_recipe
    assert 'ingredients' in inserted_recipe

# Test de la fonction de récupération de toutes les recettes
def test_get_recipes():
    recipes_collection = get_test_collection()

    # Insertion de recettes d'exemple pour le test
    sample_data = [
        {"name": "Recette 1", "ingredients": ["Ingrédient A", "Ingrédient B"]},
        {"name": "Recette 2", "ingredients": ["Ingrédient C", "Ingrédient D"]}
    ]
    for data in sample_data:
        create_recipe(data, recipes_collection)

    # Appel de la fonction de récupération de toutes les recettes
    recipes = get_recipes(recipes_collection)

    # TODO : Vérification que le nombre de recettes récupérées est supérieur ou égal à 2
    len(recipes) >= 2

# Test de la fonction de récupération d'une recette par son ID
def test_get_recipe():
    recipes_collection = get_test_collection()

    # Insertion d'une recette d'exemple pour le test
    sample_data = {
        "name": "Recette Exemple",
        "ingredients": ["Ingrédient 1", "Ingrédient 2"]
    }
    inserted_recipe = create_recipe(sample_data, recipes_collection)

    # Appel de la fonction de récupération de la recette par son ID
    recipe = get_recipe(inserted_recipe['_id'], recipes_collection)

    # Vérification du nom et des ingrédients de la recette récupérée
    assert recipe['name'] == sample_data['name']
    assert recipe['ingredients'] == sample_data['ingredients']

# Test de la fonction de mise à jour d'une recette par son ID
def test_update_recipe():
    recipes_collection = get_test_collection()

    # Insertion d'une recette d'exemple pour le test
    sample_data = {
        "name": "Recette Exemple",
        "ingredients": ["Ingrédient 1", "Ingrédient 2"]
    }
    inserted_recipe = create_recipe(sample_data, recipes_collection)

    # Données de mise à jour
    update_data = {
        "name": "Nom de la Recette Mis à Jour",
        "ingredients": ["Ingrédient 10", "Ingrédient 20"]
    }

    # Appel de la fonction de mise à jour de la recette
    updated_recipe = update_recipe(inserted_recipe['_id'], update_data, recipes_collection)

    # Vérification du nom et des ingrédients de la recette mise à jour
    assert updated_recipe['name'] == update_data['name']
    assert updated_recipe['ingredients'] == update_data['ingredients']

# Test de la fonction de suppression d'une recette par son ID
def test_delete_recipe():
    recipes_collection = get_test_collection()

    # Insertion d'une recette d'exemple pour le test
    sample_data = {
        "name": "Recette Exemple",
        "ingredients": ["Ingrédient 1", "Ingrédient 2"]
    }
    inserted_recipe = create_recipe(sample_data, recipes_collection)

    # Appel de la fonction de suppression de la recette
    result = delete_recipe(inserted_recipe['_id'], recipes_collection)

    # Vérification du nombre de recettes supprimées (doit être égal à 1)
    assert result.deleted_count == 1
