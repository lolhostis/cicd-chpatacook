import sys
sys.path.append("../../back")  # Add the parent directory to the Python path

from database import *
import pytest
from os import environ

def test_successful_db_connection():
    # Attempt to connect to the database using the DATABASE_URL environment variable
    client = connect_to_db(environ.get("DATABASE_URL", "mongodb://localhost:27017/"))
    
    # Check if the connection is successful (client should not be None)
    # TODO: vérifier que le client n'est pas nul

def test_failed_db_connection():
    # Attempt to connect to the database using an invalid connection string
    
    # TODO: vérifier qu'une exception est bien levée quand une connection string erronée est passée en paramètre
    pass
