import sys
sys.path.append("../../back")  # Ajoute le répertoire parent au chemin d'accès Python
from recipes import *
from main import *
from database import *
import json

# Test de la route de création de recette
def test_create_recipe_route():
    client = app.test_client()

    # Données d'exemple pour la recette
    sample_data = {
        "name": "Recette Exemple",
        "ingredients": ["Ingrédient 1", "Ingrédient 2"]
    }

    # Envoi d'une requête POST pour créer la recette
    response = client.post('/recipes', json=sample_data)

    # TODO: vérifier que le code de statut HTTP est bien 201 (201 pour "Created")
    assert response.status_code == 201

    # Vérification des données de réponse
    response_data = json.loads(response.data)
    # TODO: vérifier que le nom de la recette renvoyée correspond bien au nom de la recette envoyée
    response_data['name'] == sample_data['name']
    # TODO: vérifier que les ingrédients de la recette renvoyée correspondent bien aux ingrédients envoyés
    response_data['ingredients'] == sample_data['ingredients']

# Test de la route de récupération de toutes les recettes
def test_get_recipes_route():
    client = app.test_client()

    # Envoi d'une requête GET pour récupérer toutes les recettes
    response = client.get('/recipes')

    # Vérification du code de statut HTTP (200 pour "OK")
    # TODO: vérifier que le code de statut HTTP est bien 200 (200 pour "OK")
    assert response.status_code == 200

    # Vérification que les données de réponse sont une liste
    response_data = json.loads(response.data)
    # TODO: vérifier que les données de retour est bien une liste
    assert type(response_data) == list

# Test de la route de récupération d'une recette par son ID
def test_get_recipe_route():
    client = app.test_client()

    # Données d'exemple pour la recette
    sample_data = {
        "name": "Recette Exemple",
        "ingredients": ["Ingrédient 1", "Ingrédient 2"]
    }

    # Création d'une recette d'exemple
    response = client.post('/recipes', json=sample_data)
    # TODO: vérifier que le code de statut HTTP est bien 201
    assert response.status_code == 201

    response_data = json.loads(response.data)

    # Récupération de la recette créée par son ID
    response = client.get(f"/recipes/{response_data['_id']}")

    # Vérification du code de statut HTTP (200 pour "OK")
    # TODO: vérifier que le code de statut HTTP est bien 200
    assert response.status_code == 200

    # Vérification de la présence des champs "name" et "ingredients" dans les données de réponse
    response_data = json.loads(response.data)
    assert "name" in response_data
    assert "ingredients" in response_data

# Test de la route de mise à jour d'une recette par son ID
def test_update_recipe_route():
    client = app.test_client()

    # Données d'exemple pour la recette
    sample_data = {
        "name": "Recette Exemple",
        "ingredients": ["Ingrédient 1", "Ingrédient 2"]
    }

    # Création d'une recette d'exemple
    response = client.post('/recipes', json=sample_data)
    # TODO: vérifier que le code de statut HTTP est bien 201
    assert response.status_code == 201
    
    response_data = json.loads(response.data)

    # Données de mise à jour
    update_data = {
        "name": "Nom de la Recette Mis à Jour",
        "ingredients": ["Ingrédient 10", "Ingrédient 20"]
    }

    # Envoi d'une requête PUT pour mettre à jour la recette créée
    response = client.put(f"/recipes/{response_data['_id']}", json=update_data)

    # Vérification du code de statut HTTP (200 pour "OK")
    # TODO: vérifier que le code de statut HTTP est bien 200
    assert response.status_code == 200

    # Vérification des données de réponse
    response_data = json.loads(response.data)
    assert response_data["name"] == update_data["name"]
    assert response_data["ingredients"] == update_data["ingredients"]

# Test de la route de suppression d'une recette par son ID
def test_delete_recipe_route():
    client = app.test_client()

    # Données d'exemple pour la recette
    sample_data = {
        "name": "Recette Exemple",
        "ingredients": ["Ingrédient 1", "Ingrédient 2"]
    }

    # Création d'une recette d'exemple
    response = client.post('/recipes', json=sample_data)
    
    # TODO: vérifier que le code de statut HTTP est bien 201
    assert response.status_code == 201

    response_data = json.loads(response.data)

    # Envoi d'une requête DELETE pour supprimer la recette créée
    response = client.delete(f"/recipes/{response_data['_id']}")

    # Vérification du code de statut HTTP (204 pour "No Content")
    # TODO: vérifier que le code de statut HTTP est bien 204 (204 pour "No Content")
    assert response.status_code == 204
