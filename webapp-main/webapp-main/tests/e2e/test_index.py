from playwright.sync_api import sync_playwright
from os import environ

# Définition de la fonction de test pour la section "Dernières Recettes" et le bouton "Ajouter une Recette"
def test_latest_recipes_and_add_recipe_button():
    with sync_playwright() as p:
        browser = p.chromium.launch()
        context = browser.new_context()
        page = context.new_page()
        
        # Navigation vers l'URL de l'application frontend
        page.goto(environ.get("FRONTEND_URL", "http://localhost:5001"))

        # Vérification de la présence de la section "Dernières Recettes"
        latest_recipes_section = page.locator('h2:has-text("Latest Recipes:")')
        assert latest_recipes_section.is_visible()

        # Vérification de la présence et du clicabilité du bouton "Add a recipe"
        add_recipe_button = page.locator('a[href="/add_recipe"] >> button')
        assert add_recipe_button.is_visible()
        add_recipe_button.click()

        # Fermeture du contexte et du navigateur
        context.close()
        browser.close()

# Définition de la fonction de test pour la section "Recette Aléatoire"
def test_random_recipe_section():
    with sync_playwright() as p:
        browser = p.chromium.launch()
        context = browser.new_context()
        page = context.new_page()
        
        # Navigation vers l'URL de l'application frontend
        page.goto(environ.get("FRONTEND_URL", "http://localhost:5001"))

        # Vérification de la présence de la section "Recette Aléatoire"
        random_recipe_section = page.locator('h2:has-text("Random Recipe:")')
        assert random_recipe_section.is_visible()
        
        # Fermeture du contexte et du navigateur
        context.close()
        browser.close()

# Définition de la fonction de test pour la section "Recette Aléatoire"
def test_tile():
    with sync_playwright() as p:
        browser = p.chromium.launch()
        context = browser.new_context()
        page = context.new_page()
        
        # Navigation vers l'URL de l'application frontend
        page.goto(environ.get("FRONTEND_URL", "http://localhost:5001"))

        # TODO: vérifier que le titre de la page en H1 contient bien "Random Recipe"
        
        # Fermeture du contexte et du navigateur
        context.close()
        browser.close()
