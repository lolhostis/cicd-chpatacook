from playwright.sync_api import sync_playwright
from os import environ

# Définition de la fonction de test
def test_add_recipe_form():
    # Initialisation de Playwright
    with sync_playwright() as p:
        # Lancement du navigateur Chromium
        browser = p.chromium.launch()
        # Création d'un nouveau contexte de navigation
        context = browser.new_context()
        # Création d'une nouvelle page dans le contexte
        page = context.new_page()

        # Récupération de l'URL de l'application frontend à tester
        url = environ.get("FRONTEND_URL", "http://localhost:5001") + "/add_recipe"
        # Navigation vers l'URL spécifiée
        page.goto(url)

        # Vérification de la présence du formulaire "Add Recipe"
        add_recipe_form = page.locator('h1:has-text("Add Recipe")')
        assert add_recipe_form.is_visible()

        # Définition du nom de la recette de test
        recipe_name = "Recette de Test"

        # Remplissage du formulaire
        page.fill('#name', recipe_name)
        page.fill('#ingredients', 'Ingrédient 1, Ingrédient 2')

        # Soumission du formulaire
        submit_button = page.locator('input[type="submit"]')
        submit_button.click()

        # Navigation vers la page d'accueil
        page.goto(environ.get("FRONTEND_URL", "http://localhost:5001"))

        # Vérification de la visibilité de la recette ajoutée dans la section "Latest Recipes"
        added_recipe_elements = page.locator(f'li:has-text("{recipe_name}")').all()
        for element in added_recipe_elements:
            assert element.is_visible()

        # Fermeture du contexte et du navigateur
        context.close()
        browser.close()
